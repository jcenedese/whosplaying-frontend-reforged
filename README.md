# WhosplayingFrontend

Petit projet front développé sur mon temps libre. Il a pour but d'apprendre aux joueurs de League of Legends (ou les amateur d'e-sport) à jouer un champion en particulier en proposant à l'utilisateur de regarder des parties en live (pour l'instant, vous êtes limités au serveur EUW)

## Prérequis:

- Node 13.5.0
- Angular 8

## Usage

Récupérer et démarrer [le projet backend](https://gitlab.com/jcenedese/whosplaying-backend).

Démarrer le serveur local à l'aide de la commande `npm start`

Se rendre à l'adresse [localhost:4200](http://localhost:4200)
